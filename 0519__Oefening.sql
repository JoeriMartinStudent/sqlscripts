USE modernways;

ALTER TABLE liedjes ADD COLUMN Genre VARCHAR(20);

UPDATE liedjes SET genre = 'Hard Rock'
WHERE Artiest ='Led Zeppelin' OR Artiest ='Van Halen';