USE modernways;
CREATE TABLE Metingen(
	Tijdstip DATETIME	NOT NULL,
	Grootte SMALLINT NOT NULL,
	Marge DOUBLE(2,2) NOT NULL
);