USE modernways;
CREATE TABLE Huisdieren(
	Naam VARCHAR(100) CHAR SET UTF8MB4 NOT NULL,
	Leeftijd SMALLINT NOT NULL,
	Soort VARCHAR(50) NOT NULL
);