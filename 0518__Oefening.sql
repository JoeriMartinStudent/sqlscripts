USE modernways;

ALTER TABLE huisdieren ADD COLUMN geluid VARCHAR(20) CHAR SET UTF8MB4; 

UPDATE huisdieren SET geluid = 'WAF!'
WHERE Soort = 'hond'

UPDATE huisdieren SET geluid = 'miauwww...'
WHERE Soort = 'kat';