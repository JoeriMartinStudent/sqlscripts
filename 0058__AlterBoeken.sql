ALTER TABLE boeken
ADD COLUMN personen_Id INT,
ADD CONSTRAINT fk_Boeken_Personen
	FOREIGN KEY (Personen_Id)
	REFERENCES personen(Id);