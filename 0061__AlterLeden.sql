USE modernways;
ALTER TABLE leden
ADD COLUMN Taken_Id INT,
ADD CONSTRAINT fk_leden_Taken
FOREIGN KEY (Taken_Id)
REFERENCES taken(Id);